#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from distutils.core import setup
from setuptools import find_packages
import os
import io
import sys
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
logger.addHandler(ch)

opts = {}
args_ = ('--android-api', '--storage-dir', '--dist-name', '--arch',
    '--local_recipes')
for par in sys.argv:
    for arg_ in args_:
        if par.startswith(arg_):
            opts[arg_] = par[len(arg_) + 1:]

options = {
    'apk': {
        'permissions': ['INTERNET', 'ACCESS_NETWORK_STATE', 'CAMERA'],
        'presplash-color': 'white'}}

if '--local-recipes' not in opts.keys():
    local_recipes_ = os.environ.get('P4A_LOCAL_RECIPES', '../recipes')
    options['apk']['local-recipes'] = local_recipes_
    logger.info('... Local recipes: %s' % local_recipes_)

if '--android-api' in opts.keys():
    api_ = opts.get('--android-api')
else:
    api_ = os.environ.get('ANDROIDAPI') or '21'
    options['apk']['android-api'] = api_

if '--storage-dir' not in opts.keys():
    if 'P4A_STORAGE_DIR' in os.environ.keys():
        options['apk']['storage-dir'] = os.environ.get('P4A_STORAGE_DIR')
        logger.info('... Storage dir: %s' % options['apk']['storage-dir'])

if '--arch' in opts.keys():
    arch_ = opts.get('arch')
else:
    arch_ = 'armeabi-v7a'
    options['apk']['arch'] = arch_

if '--dist-name' not in opts.keys():
    _, project = os.path.split(os.getcwd())
    distribution = '%s_%s_%s' % (project, arch_, api_)
    options['apk']['dist-name'] = distribution
    logger.info('... Using/creating distribution: %s' % distribution)


def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()


def _add_modules():
    result = []
    for fold in ('core_modules', 'modules'):
        for pack in find_packages('sale/%s' % fold):
            if '.' in pack:
                continue
            for subf in (
                    '*.xml', '*.py', 'tryton.cfg',
                    'icons/*.svg',
                    'locale/*.po',
                    'view/*.xml'):
                result.append('%s/%s/%s' % (fold, pack, subf))
    return result


_pack_data = {
    'sale': [
        '*.py',
        '*.kv',
        'dbmanage',
        'version',
        'etc/trytond.conf',
        'template/sale.sqlite',
        'config/own.cfg',
        'data/locale/*/LC_MESSAGES/*.mo',
        'data/locale/*/LC_MESSAGES/*.po'],
    'res': [
        'drawable/icon.png',
        'drawable/presplash_512_256.jpg']}

_pack_data['sale'].extend(_add_modules())

args = {}

try:
    from babel.messages import frontend as babel

    args['cmdclass'] = {
        'compile_catalog': babel.compile_catalog,
        'extract_messages': babel.extract_messages,
        'init_catalog': babel.init_catalog,
        'update_catalog': babel.update_catalog,
        }

    args['message_extractors'] = {
        'sale': [
            ('**.py', 'python', None),
            ('**.kv', 'python', None)],
        }

except ImportError:
    pass

setup(name='sale',
    version='1.0',
    description='The Tryvy Sale app',
    long_description=read('README.md'),
    author='Datalife S.Coop.',
    author_email='info@datalifeit.es',
    url='https://gitlab.com/datalifeit/tryvy-sale',
    license='GPL-3',
    options=options,
    packages=['sale'],
    package_data=_pack_data,
    install_requires=['tryvy'],
    **args)
