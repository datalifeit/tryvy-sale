#!/usr/bin/env python

# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
# from tryvy.tryton.call import login, logout, execute

if __name__ == '__main__':
    from tryvy.commandline import parse as parse_command_line
    options = parse_command_line().copy()
    from app import app_run
    options.pop('dev')
    app_run(**options)
