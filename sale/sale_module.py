# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from functools import partial
import datetime
from decimal import Decimal, ROUND_HALF_EVEN
from kivy.app import App
from kivy.properties import ObjectProperty, AliasProperty
from tryvy.ui.screen import TrytonTreeScreen, TrytonFormScreen
from tryvy.ui.tree import TrytonTree, TreeAvatarItem, TreeItem
import gettext
from tryvy.ui.form import FormItem, TrytonTabbedForm
from trytond.transaction import Transaction
from tryvy.tryton.call import execute as call_execute
from tryvy.ui.dialog import error_dialog

root_app = App.get_running_app()
_ = partial(gettext.dgettext, 'sale')


class SaleTreeScreen(TrytonTreeScreen):
    pass


class SaleTreeItem(TreeAvatarItem):

    def get_first_record(self):
        number = '[{}]'.format(self.record['number'].value) if self.record[
            'number'].value else ''
        return number + (' {}'.format(self.record['party.rec_name'].value))


class SaleTree(TrytonTree):
    _name = 'sale.sale'
    _fields = ['state', 'company', 'company.rec_name', 'sale_date', 'party',
        'party.rec_name', 'currency', 'shipment_address', 'invoice_address',
        'shipment_address.rec_name', 'shipment_party', 'number',
        'payment_term', 'payment_term.rec_name', 'untaxed_amount',
        'total_amount', 'tax_amount']
    item_cls = SaleTreeItem

    def get_focus_date(self):
        date_value = self.parent.filter_bar.value
        if type(date_value) == datetime.datetime:
            return date_value.date()
        else:
            return date_value

    def update_new_record(self, record):
        record['sale_date'].value = self.get_focus_date()


class SaleTabbedForm(TrytonTabbedForm):
    pass


class SaleItem(FormItem):
    _name = SaleTree._name
    _fields = SaleTree._fields

    def on_change_party(self):
        if self.record['party'].value:
            addresses = call_execute('model', 'party.address', 'search_read', [
                ('party', '=', self.record['party'].value),
                ('active', '=', True)], 0, None, None,
                ['id', 'rec_name', 'invoice', 'delivery'])
            for address in addresses:
                if 'invoice' in address:
                    self.record['invoice_address'].value = address['id']
                    self.record['invoice_address.rec_name'] = address[
                        'rec_name']
                if 'delivery' in address:
                    self.record['shipment_address'].value = address['id']
                    self.record['shipment_address.rec_name'].value = address[
                        'rec_name']
            self.record['payment_term'].value = self.record[
                'party'].get_attribute('customer_payment_term')
            self.record['payment_term.rec_name'].value = self.record[
                'party'].get_attribute('customer_payment_term.rec_name')

    def quote(self):
        try:
            self.record.save()
            call_execute('model', self._name, 'quote',
                [self.record['id'].value])
        except Exception as e:
            error_dialog(title=_('Server error!'),
                text=_('Cannot Quote Sale!'), exception=e)
        else:
            self._reload()
            root_app.root_layout.configure_toolbar()

    def edit(self, screen):
        try:
            self.record.save()
            call_execute('model', self._name, 'draft',
                [self.record['id'].value])
        except Exception as e:
            error_dialog(title=_('Server error!'),
                text=_('Cannot back to Draft Sale!'), exception=e)
        else:
            self._reload()
            root_app.root_layout.configure_toolbar()


class SaleLineTreeItem(TreeItem):
    pass


class SaleLineTree(TrytonTree):
    _name = 'sale.line'
    _limit = 20
    _fields = ['sale', 'type', 'product', 'product.rec_name', 'quantity',
        'description', 'sale_state', 'unit_price', 'sale.currency', 'amount',
        'unit', 'unit.rec_name', 'sale.party', 'sale.sale_date',
        'product.sale_uom', 'sale.currency.rounding']
    _list_field = 'lines'
    item_cls = SaleLineTreeItem

    def update_new_record(self, record):
        record.add_parent('sale', self.parent.parent_widget.record)
        if record['sale'].value < 0:
            record['sale_state'].value = 'draft'
        else:
            record['sale_state'].value = record['sale'].get_attribute('state')


class SaleLineItem(FormItem):
    _name = SaleLineTree._name
    _fields = SaleLineTree._fields

    def on_change_product(self):
        if self.record['product'].value:
            self.record['type'].value = 'line'
            self.record['description'].value = self.record[
                'product.rec_name'].value
            self.record['unit'].value = self.record[
                'product'].get_attribute('sale_uom')
            self.record['unit.rec_name'].value = self.record[
                'product'].get_attribute('sale_uom.rec_name')
            self.record['unit_price'].value = self.record[
                'product'].get_attribute('list_price')
            if self.record['unit_price'].value:
                self.record['unit_price'].value = self.record[
                    'unit_price'].value.quantize(
                        Decimal(1) / 10 ** self.record['unit_price]'].digits)
            self.record['amount'] = self.on_change_with_amount()

    def on_change_with_amount(self):
        if self.record['type'].value == 'line':
            currency = self.record['sale.currency'].value if self.record[
                'sale'].value else None
            amount = Decimal(str(
                self.record['quantity'].value or '0.0')) * (
                self.record['unit_price'].value or Decimal('0.0'))
            if currency:
                return self.currency_round(amount)
            return amount
        return Decimal('0.0')

    def currency_round(self, amount, rounding=ROUND_HALF_EVEN):
        currency_rounding = self.record['sale.currency.rounding'].value
        return (amount / currency_rounding).quantize(Decimal('1.'),
                rounding=rounding) * currency_rounding

    def on_change_quantity(self):
        self.record['amount'].value = self.on_change_with_amount()
        if not self.record['product'].value:
            return
        with Transaction().set_context(self._get_context_sale_price()):
            self.record['unit_price'].value = 0
            if self.record['unit_price'].value:
                self.record['unit_price'].value = 0

    def on_change_unit(self):
        self.on_change_quantity()

    def _get_context_sale_price(self):
        context = {}
        if self.record['sale'].value:
            if self.record['sale.currency'].value:
                context['currency'] = self.sale.currency.id
            if self.record['sale.party'].value:
                context['customer'] = self.sale.party.id
            if self.record['sale.sale_date'].value:
                context['sale_date'] = self.sale.sale_date
        if self.unit:
            context['uom'] = self.record['unit'].value
        else:
            context['uom'] = self.record['product.sale_uom'].value
        return context


class SaleFormScreen(TrytonFormScreen):
    tab_panel = ObjectProperty(None)
    sale_item = ObjectProperty(None)

    def on_pre_enter(self):
        super(SaleFormScreen, self).on_pre_enter()
        prop = self.property('readonly')
        prop.dispatch(self)

    def get_readonly(self):
        if not self.sale_item.record['state']:
            return False
        return self.sale_item.record['state'].value not in ('draft', '')

    readonly = AliasProperty(get_readonly, rebind=True)

    def get_toolbar_actions(self):
        left, right = super(SaleFormScreen,
            self).get_toolbar_actions()
        if not self.readonly:
            right.insert(0, ['content-save',
                lambda x: self.tryton_view.save()])
        elif len(right) == 2:
            del right[0]
        if self.sale_item.record['state'].value == 'quotation':
            right.insert(0, ['pencil',
                lambda x: self.tryton_view.ids.tab_manager.get_screen('sale'
                    ).tryton_view.edit(self)])
        return left, right


class SaleItemExtraInfo(FormItem):
    _name = SaleTree._name
