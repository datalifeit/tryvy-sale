# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
import sys
import kivy
from functools import partial
from kivy.app import App
from tryvy.app import TrytonApp, TrytonLayout
from tryvy.ui.navdrawer import TrytonNavLayout
import gettext
root_app = App.get_running_app()

_ = partial(gettext.dgettext, 'sale')

__all__ = ['app_run']

kivy.require('1.8.0')
DEFAULT_TEMPLATE_ICON = 'test-tube'


class AppLayout(TrytonLayout):
    pass


class SaleApp(TrytonApp):
    _config_file_name = 'etc/trytond.conf'
    _textdomains = [(unicode(os.path.dirname(__file__),
        sys.getfilesystemencoding()), 'sale'), ]
    _employee = None

    def get_company_info(self):
        data = {}
        with open("version") as lines:
            for line in lines:
                if '#' not in line:
                    parts = line.replace("\"", "").split(" = ")
                    data[parts[0]] = parts[1]
        return data

    def build(self):
        global root_app
        root_app = self
        super(SaleApp, self).build()
        self.theme_cls.primary_palette = 'BlueGrey'
        self.theme_cls.accent_palette = 'Orange'
        root = TrytonNavLayout()
        self.nav_layout = root
        self.root_layout = layout = AppLayout(root_app=self)
        root.add_widget(layout)
        self.title = _('Sale')
        company_info = self.get_company_info()
        self.company_info = company_info['__name__']
        self.company_version = company_info['__version__']
        self.company_description = company_info['__description__']
        return root

    def build_config(self, config):
        config.setdefaults('Settings', {
            'server': 'tryton.datalifeit.es',
            'port': 8050,
            'database': 'trytontest_saleapk',
            'uid': 'dev1',
            'pwd': 'device1',
            'load_employees': True,
            'repl_interval': 60,
        })

    def _(self, value):
        # use only in kv side
        return gettext.dgettext('sale', value)

    @classmethod
    def get_instance(cls):
        return root_app

    def rebind_record(self, record=None, _reload=False, instances=[]):
        for instance in instances:
            prop = instance.property('record')
            prop.dispatch(instance)


def app_run(**kwargs):

    # Init client App
    kwargs['database'] = 'sale'
    SaleApp(**kwargs).run()
