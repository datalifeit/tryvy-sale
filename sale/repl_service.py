# The COPYRIGHT file at the top level of
# # this repository contains the full copyright notices and license terms.
from tryvy.tryton.repl_service import run_repl_service
from kivy import platform

if __name__ == '__main__':
    if platform == 'android':
        run_repl_service()
