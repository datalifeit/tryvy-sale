Sale
========

Sale is the tryvy powered Sale app. 


Installation
============

To install Sale, clone the project and run the setup.py script. The following line works on Linux, other OS not tested:

    sudo ./setup.py install

APK Build
=========

To Build Sale apk, clone the project and run the setup.py script. The following line works on Linux, other OS not tested:

    ./setup.py apk


License
=======

See LICENSE file
